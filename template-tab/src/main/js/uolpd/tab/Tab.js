function Tab(configTab) {
    var self = this;

    var flagTimeOpen, flagTimeClose, IFRAME_ID, WRAPPER_ID;
    var config = configTab || {};
    var isOpened = false;

    function configureNames (config) {
        IFRAME_ID = 'uol-iframe-banner-' + config.title;
        WRAPPER_ID = 'uol-richmedia-' + config.title;
    }

    function setCustomEvents () {
        var iframe = document.getElementById(IFRAME_ID);
        iframe.addEventListener('mouseenter', function(e) {
            e.stopPropagation();
            clearTimeout(flagTimeClose);
            if (isOpened === false) {
                flagTimeOpen = setTimeout(function() {
                    open();
                    isOpened = true;
                }, config.format.timeToOpen);
            }
        });

        iframe.addEventListener('mouseleave', function(e) {
            e.stopPropagation();
            clearTimeout(flagTimeOpen);
            if (config.format.reload === false && isOpened === true) {
                flagTimeClose = setTimeout(function() {
                    close();
                    isOpened = false;
                }, config.format.timeToClose);
            }

            if (config.format.reload === true) {
                reloadIframe(IFRAME_ID);
                if (isOpened === true) {
                    self.track('close', config.track.close);
                }
                setTimeout(function () {
                    reload();
                    isOpened = false;
                }, config.format.timeToReload);
            }
        });
    }

    function open () {
        setClipRect('expand');
        self.track('open', config.track.open);
    }

    function close () {
        setClipRect('retract');
        self.track('close', config.track.close);
    }

    function reload () {
        setClipRect('retract');
        writeIframeContent(IFRAME_ID, config.clientSource);
    }

    function setClipRect (action) {
        var rect = defineDirection(config.format.direction, action);
        var iframe = document.getElementById(IFRAME_ID);
        iframe.style.clip = 'rect(' + rect.top + 'px ' + rect.right + 'px ' + rect.bottom + 'px ' + rect.left + 'px)';
    }

    function defineDirection (direction, action) {
       var directions = {
            'TOP': {
                'right': config.format.width,
                'bottom': config.format.expanded.height,
                'left':0,
                'top': (action === 'expand') ? 0 : (config.format.expanded.height-config.format.initial.height)
            },
            'DOWN': {
                'top': 0,
                'right': config.format.width,
                'bottom': (action === 'expand') ? config.format.expanded.height : config.format.initial.height,
                'left':0,
            }
        };
        return directions[direction];
    }

    self.track = function (params, id) {
        var url, path;
        var timestamp = (new Date()).getTime();
        var imgTracker = new Image();
        path = "http://www5.smartadserver.com/imp?imgid=";

        if (id === "" || id === undefined) {
            return;
        }

        url = path + id + "&tmstp=" + timestamp + "&tgt=&" + params;
        imgTracker.src = url;
    };

    self.createFirstContent = function (retractHeight, width, height) {
        var iframe = document.createElement('iframe');
        iframe.setAttribute('id', IFRAME_ID);
        iframe.setAttribute('width', width);
        iframe.setAttribute('height', height);
        iframe.setAttribute('marginwidth', '0');
        iframe.setAttribute('marginheight', '0');
        iframe.setAttribute('frameborder', '0');
        iframe.setAttribute('allowtransparency', 'true');
        iframe.setAttribute('scrolling', 'no');
        iframe.style.position = 'absolute';
        iframe.style.zIndex = 6000000;

        if(config.format.direction === 'TOP') {
            iframe.style.clip = 'rect('+retractHeight+'px '+width+'px '+height+'px 0)';
            iframe.style.top = -(config.format.expanded.height-config.format.initial.height)+'px';
        } else if(config.format.direction === 'DOWN') {
            iframe.style.clip = 'rect(0px '+width+'px '+config.format.initial.height+'px 0)';
            iframe.style.top = '0px';
        }
        return iframe;
    };

    self.getClickTag  = function (value) {
        var index = (value === undefined) ? 0 : value;
        return config.clickTag[index];
    };

    function reloadIframe (id) {
        var iframe = document.getElementById(id);
        iframe.contentWindow.location.reload();
    }

    function writeIframeContent (id, src) {
        var iframe = document.getElementById(id);
        iframe.contentWindow.document.open();
        iframe.contentWindow.document.write(src);
        iframe.contentWindow.document.close();
    }

    function createMainContainer () {
        var div = document.createElement('div');
        div.setAttribute('id', WRAPPER_ID);
        div.style.position = 'relative';
        return div;
    }

    self.render = function () {
        var container = document.getElementById(config.bannerId);
        if (!container) {
            return;
        }
        configureNames(config);

        var width = config.format.width;
        var heightExp = config.format.expanded.height;
        var heightInitial = config.format.initial.height;
        var retractHeight = (heightExp-heightInitial);
        var iframe = self.createFirstContent(retractHeight, width, heightExp);

        var div = createMainContainer();
        div.appendChild(iframe);
        container.appendChild(div);
        setCustomEvents();
        writeIframeContent(IFRAME_ID, config.clientSource);
    };
}