function Config () {
    return {
        title : '%%TITLE%%',
        format : {
            width : '%%WIDTH_INITIAL%%',
            initial : {
                height : '%%HEIGHT_INITIAL%%'
            },
            expanded : {
                height : '%%HEIGHT_EXPANDED%%'
            },
            reload : '%%RELOAD%%',
            timeToReload : '%%TIME_RELOAD%%',
            timeToOpen : '%%TIME_OPEN%%',
            timeToClose : '%%TIME_CLOSE%%',
            direction : '%%DIRECTION%%'
        },
        clickTag : [
            '%%CLICKTAG%%'
        ],
        track : {
            open  : '%%ID_OPEN%%',
            close : '%%ID_CLOSE%%'
        },
        bannerId : '%%BANNER_ID%%',
        clientSource : '%%CLIENTSRC%%'
    };
}