module.exports = function (grunt) {
    var dirs = require('./dirs');

    return {
        options: {
            banner: '(function (window, document, undefined){\n\'use strict\';\n',
            footer: '\n})(this, document);'
        },
        tab: {
            src: [
                '<%= dirs.src %>/uolpd/tab/Config.js',
                '<%= dirs.src %>/uolpd/tab/Tab.js',
                '<%= dirs.src %>/uolpd/tab/Init.js'
            ],
            dest: '<%= dirs.build %>/template-tab.js'
        }
    };
};
