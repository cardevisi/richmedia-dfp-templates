module.exports = function (grunt) {
    var dirs = require('./dirs');

    return {
        dist: {
            src: '<%= dirs.htmlSrc %>/index.html',
            dest: '<%= dirs.build %>/index.html',
            options: {
                beautify: true,
                sections: {
                    layout: {
                        header: '<%= dirs.htmlSrc %>/header.html',
                        body: '<%= dirs.htmlSrc %>/body.html'
                    }
                },
                data: {
                    script: 'template-tab.js',
                    title: "UOL Richmedia Template TAB"
                }
            }
        }
    };
};