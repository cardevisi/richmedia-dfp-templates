var Assert = {
    assertLogMessage: function (messages, message) {
        var prefix = '[UOLPD.TagManager.DfpAsync] ';

        if (!messages || !messages.length) {
            console.error('[Assert.assertLogMessage] Erro ao tentar validar: ' + message);
        }

        var notFound = 0;
        for (var i = 0; i < messages.length; i++) {
            var messageRegex = new RegExp(message, 'gm');

            if (messageRegex.test(messages[i])) {
                notFound++;
                expect(messages[i]).toEqual(prefix + message);
                return;
            }
        }

        if (notFound === 0) {
            throw new Error('A mensagem "' + prefix + message + '" não foi encontrada no histórico de logs');
        }
    },

    assertTrue: function (argument) {
        expect(argument).toBeTruthy();
    },

    assertFalse: function (argument) {
        expect(argument).toBeFalsy();
    },

    assertDefined: function (argument) {
        expect(argument).toBeDefined();
    },

    assertUndefined: function (argument) {
        expect(argument).toBeUndefined();
    },

    assertToBe: function (argument, original) {
        expect(argument).toBe(original);
    },

    assertEqual: function (argument, original) {
        expect(argument).toEqual(original);
    },

    assertEqualAny: function (argument, type) {
        expect(argument).toEqual(jasmine.any(type));
    }
};