function Config () {
    return {
        debug: true, //habilita o modo visual das mascaras
        title : '%%TITLE%%', //nome da campanha ou peca
        format: {
            initial: {
                width : '%%WIDTH_INITIAL%%', // largura do formato inicial
                height : '%%HEIGHT_INITIAL%%', // altura do formato inicial
                mask : '%%MASK_INITIAL%%', // em true habilita a mascara - false desabilita
                background : '@@%%BACKGROUND_INITIAL%%', // cor de fundo Ex. #000000
                src : '@@%%SOURCE_INITIAL%%', //path do html ou variavel do conteudo html
                reload: false //recarrega a primeira peca
            },
            expanded : {
                width : '%%WIDTH_EXPANDED%%', //largura do formato expandido
                height : '%%HEIGHT_EXPANDED%%', //altura do formato expandido
                mask : '%%MASK_EXPANDED%%', // em true habilita a mascara - false desabilita
                background : '@@%%BACKGROUND_EXPANDED%%',
                src : '@@%%SOURCE_EXPANDED%%', //path do html ou variavel do conteudo html
                align : '@@%%ALIGN%%', //TOP_LEFT - TOP_RIGHT - BOTTOM_LEFT - BOTTOM_RIGHT - CENTER_CENTER
                margin : {
                    x: '%%MARGIN_X%%', //deslocamento em x
                    y: '%%MARGIN_Y%%' //deslocamento em y
                },
                reload: false //recarrega a segunda peca
            },
            write: false // em false utilizado o modo de carregamento via src - true em modo write
        },
        clickTag : [
            '%%CLICKTAG%%'
        ],
        track : {
            open : '%%ID_OPEN%%',
            close : '%%ID_CLOSE%%'
        },
        bannerId : '@@%%BANNER_ID%%' // banner ID do container
    };
}