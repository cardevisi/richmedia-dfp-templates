function Mosaico (configMosaico) {
    var self = this;

    var FIRST_IFRAME, SECOND_IFRAME, FIRST_CONTAINER, SECOND_CONTAINER, PRELOAD_CONTAINER, WRAPPER_ID;
    var config = configMosaico || {};
    var elementAlign = new Align();

    function configureNames () {
        FIRST_IFRAME = 'uol-iframe-first-content-' + config.title;
        SECOND_IFRAME = 'uol-iframe-second-content-' + config.title;
        FIRST_CONTAINER = 'first-container-' + config.title;
        SECOND_CONTAINER = 'second-container-' + config.title;
        PRELOAD_CONTAINER = 'preload-container-' + config.title;
        WRAPPER_ID = 'uol-richmedia-' + config.title;
    }

    function addCssAnimation () {
        var style = document.createElement('style');
        style.setAttribute('type', 'text/css');
        var css = '@keyframes fadeIn {from {opacity: 0;}to {opacity: 1;}} @keyframes fadeOut {from {opacity: 1;}to {opacity: 0;}} .fadeIn {animation: fadeIn 1s;-webkit-animation: fadeIn 1s;-moz-animation: fadeIn 1s;-o-animation: fadeIn 1s;} .fadeOut {animation: fadeOut 0.5s;-webkit-animation: fadeOut 0.5s;-moz-animation: fadeOut 0.5s;-o-animation: fadeOut 0.5s;}';
        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }
        var head = document.head || document.getElementsByTagName('head')[0];
        head.appendChild(style);
    }

    self.getConfigTitle = function () {
        return config.title.toUpperCase();
    };

    self.clickTag = function () {
        self.close();
        window.open(self.getClickTag(), '_blank');
    };

    self.getClickTag = function (value) {
        var index = (value === undefined) ? 0 : value;
        return config.clickTag[index];
    };

    self.loadComplete = function (element) {
        var id = element.getAttribute('id');
        removePreloader(id);
    };

    self.loadError = function() {};

    self.open = function() {
        refreshHome('pause');
        fadeIn();
        toogleVisibility();
        loadIframeContent(SECOND_IFRAME, config.format.expanded.src);
        track('open', config.track.open);
    };
    
    self.close = function() {
        if (config.format.initial.reload.toString() === 'true') {
            reloadIframeContent(FIRST_IFRAME);
            setTimeout(function () {
                loadIframeContent(FIRST_IFRAME, config.format.initial.src);
            }, 500);
        }
        if (config.format.expanded.reload.toString() === 'true') {
            reloadIframeContent(SECOND_IFRAME);
        }
        refreshHome('start');
        fadeOut();
        setTimeout(function () {
            toogleVisibility();
        }, 500);
        track('close', config.track.close);
    };

    function fadeIn () {
        var div = document.getElementById(SECOND_CONTAINER);
        var classValue = div.getAttribute('class');
        if (!classValue) {
            classValue = '';
        }

        if (classValue === SECOND_CONTAINER + ' fadeOut') {
            classValue = classValue.replace('fadeOut', 'fadeIn');
        } else {
            classValue += SECOND_CONTAINER + ' fadeIn';
        }

        div.setAttribute('class', classValue);
    }

    function fadeOut () {
        var div = document.getElementById(SECOND_CONTAINER);
        var classValue = div.getAttribute('class');
        classValue = classValue.replace('fadeIn', 'fadeOut');
        div.setAttribute('class', classValue);
    }

    function refreshHome (value) {
        if (window.homeUOL === undefined) {
            return;
        }
        if (window.homeUOL.modules === undefined) {
            return;
        }
        if (window.homeUOL.modules.refresh === undefined) {
            return;
        }
        if(value === 'pause') {
            window.homeUOL.modules.refresh.pause();
        } else if(value === 'start') {
            window.homeUOL.modules.refresh.start();
        }
    }

    function reloadIframeContent (id) {
        var iframe = document.getElementById(id);
        if(config.format.write.toString() === 'false') {
            iframe.setAttribute('src', '');
        } else {
            iframe.contentWindow.document.open();
            iframe.contentWindow.document.write('');
            iframe.contentWindow.document.close();
        }
    }

    function toogleVisibility () {
        var div = document.getElementById(SECOND_CONTAINER);
        div.style.display = (div.style.display === 'none') ? 'block' : 'none';
    }

    function track (params, id) {
        var url, path;
        var timestamp = (new Date()).getTime();
        var imgTracker = new Image();
        path = "http://www5.smartadserver.com/imp?imgid=";

        if (id === "" || id === undefined) {
            return;
        }

        url = path+id+"&tmstp="+timestamp+"&tgt=&"+params;
        imgTracker.src = url;
    }

    function createIframe (id) {
        var iframe = document.createElement('iframe');
        iframe.setAttribute('id', id);
        iframe.setAttribute('marginwidth', '0');
        iframe.setAttribute('marginheight', '0');
        iframe.setAttribute('frameborder', '0');
        iframe.setAttribute('scrolling', 'no');
        return iframe;
    }

    function sourceIframeContent(id, url) {
      var iframe = document.getElementById(id);
      iframe.setAttribute('onload', 'window.UOLI.'+self.getConfigTitle()+'.loadComplete(this);');
      iframe.setAttribute('src', url);
    }

    function writeIframeContent (id, src) {
        var iframe = document.getElementById(id);
        iframe.setAttribute('onload', 'window.UOLI.'+self.getConfigTitle()+'.loadComplete(this);');
        iframe.contentWindow.document.open();
        iframe.contentWindow.document.write(src);
        iframe.contentWindow.document.close();
    }

    function addPreloader(id) {
        var div = getContainerById(id);
        try {
            var preloadImage = createPreloader();
            div.appendChild(preloadImage);
        } catch(e) {}
    }

    function removePreloader(id) {
        var div = getContainerById(id);
        var preloadImage = document.getElementById(PRELOAD_CONTAINER);
        try{
            div.removeChild(preloadImage);
        } catch(e) {}
    }

    function getContainerById (id) {
        var div, idFound;
        if(id.indexOf('first') > -1) {
            idFound = FIRST_CONTAINER;
        } else if (id.indexOf('second') > -1) {
            idFound = SECOND_CONTAINER;
        }
        return document.getElementById(idFound);
    }

    function loadIframeContent(id, src) {
        addPreloader(id);
        if(config.format.write.toString() === 'true') {
            if (typeof src !== 'string') {
                console.log('[UOL RICHMEDIA] src em modo write incorreto');
                return;
            }
            writeIframeContent(id, src);
        } else {
            sourceIframeContent(id, src);
        }
    }

    function createCloseButtom () {
        var img = document.createElement('img');
        img.setAttribute('src', 'http://bn.imguol.com/1110/uol/cutting/fechar.gif');

        var btn = document.createElement('a');
        btn.setAttribute('value', 'Fechar');
        applyStyles(btn, {'position':'absolute','width':'55px','height':'15px','top':'0','right':'0','zIndex':3,'cursor':'pointer'});

        btn.addEventListener('click', function (e) {
            e.preventDefault();
            self.close();
        });
        btn.appendChild(img);
        return btn;
    }

    function applyStyles (element, styles) {
        for (var style in styles) {
            element.style[style] = styles[style];
        }
    }

    function createFirstContainer (src, width, height) {
        var div = createContainer(FIRST_CONTAINER);
        div.style.background = config.format.initial.background;
        applyStyles(div, {'position':'relative', 'width':width+'px', 'height':height+'px'});
        if (config.format.initial.mask.toString() === 'true') {
            div.appendChild(createMask(width, height, self.open));
        }

        var iframe = createIframe(FIRST_IFRAME);
        div.appendChild(iframe);
        applyStyles(iframe, {'position' : 'absolute',
            'width' : iframe.parentNode.style.width, 'height' : iframe.parentNode.style.height,
            'top' : iframe.parentNode.style.top, 'right' : iframe.parentNode.style.right});

        return div;
    }

    function createSecondContainer (src, width, height) {
        var div = createContainer(SECOND_CONTAINER);
        div.style.background = config.format.expanded.background;
        applyStyles(div, {'width':width+'px', 'height':height+'px', 'position':'absolute', 'display':'none', 'zIndex':6000000});
        applyStyles(div, elementAlign.getAlignments(config));
        if (config.format.expanded.mask.toString() === 'true') {
            div.appendChild(createMask(width, height, self.clickTag));
        }

        var iframe = createIframe(SECOND_IFRAME);
        div.appendChild(iframe);
        applyStyles(iframe, {'width' : iframe.parentNode.style.width,
            'position' : 'absolute', 'height' : iframe.parentNode.style.height,
            'top' : '0px', 'right' : '0px'});

        var btn = createCloseButtom();
        div.appendChild(btn);
        return div;
    }

    function createContainer (name) {
        var div = document.createElement('div');
        div.setAttribute('id', name);
        return div;
    }

    function createPreloader() {
        var img = 'http://ci.i.uol.com.br/album/wait_inv.gif';
        var image = new Image();
        applyStyles(image, {'position':'absolute', 'top':'50%', 'left':'50%', 'zIndex':1, 'margin': '-16px 0 0 -16px'});
        image.setAttribute('id', PRELOAD_CONTAINER);
        image.setAttribute('src', img);
        return image;
    }

    function createMask (width, height, callback) {
        var mask = document.createElement('a');
        applyStyles(mask, {'position':'absolute', 'width':width+'px', 'height':height+'px', 'top':'0', 'left':'0', 'zIndex':1, 'cursor':'pointer'});
        debugMask(mask);

        mask.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            callback();
        });

        return mask;
    }

    function debugMask (mask) {
        if (config.debug.toString() === 'true') {
            mask.style.background = '#ff0000';
            mask.style.opacity = 0.5;
        }
    }

    function createWrapper (name) {
        var div = createContainer(name);
        div.style.position = 'relative';
        return div;
    }

    self.render = function () {
        var container = document.getElementById(config.bannerId);
        if (!container) {
            return;
        }
        configureNames(config);
        addCssAnimation();
        var wrapper = createWrapper(WRAPPER_ID);
        var firstContainer = createFirstContainer(config.format.initial.src, config.format.initial.width, config.format.initial.height);
        wrapper.appendChild(firstContainer);
        var secondContainer = createSecondContainer(config.format.expanded.src, config.format.expanded.width, config.format.expanded.height);
        wrapper.appendChild(secondContainer);
        container.appendChild(wrapper);
        loadIframeContent(FIRST_IFRAME, config.format.initial.src);
    };

}