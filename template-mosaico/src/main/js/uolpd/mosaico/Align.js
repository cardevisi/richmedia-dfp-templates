function Align () {

    var self = this;
    
    self.getAlignments = function (config) {
        var alignments = {
            'TOP_LEFT' : {
                'top' : config.format.expanded.margin.y + 'px',
                'left' : config.format.expanded.margin.x + 'px'
            },
            'TOP_RIGHT' : {
                'top' : config.format.expanded.margin.y + 'px',
                'right' : config.format.expanded.margin.x + 'px'
            },
            'BOTTOM_LEFT' : {
                'bottom' : config.format.expanded.margin.y + 'px',
                'left' : config.format.expanded.margin.x + 'px'
            },
            'BOTTOM_RIGHT' : {
                'bottom' : config.format.expanded.margin.y + 'px',
                'right' : config.format.expanded.margin.x + 'px'
            },
            'CENTER_CENTER' : {
                'top' : '50%',
                'left' : '50%',
                'marginTop' : -((config.format.expanded.height * 0.5) - config.format.expanded.margin.y) + 'px',
                'marginLeft' : -((config.format.expanded.width * 0.5) - config.format.expanded.margin.x) + 'px'
            },
            'DEFAULT' : {
                'top' : config.format.expanded.margin.y + 'px',
                'left' : config.format.expanded.margin.x + 'px'
            }
        };

        return alignments[config.format.expanded.align] || alignments.DEFAULT;
    };
}