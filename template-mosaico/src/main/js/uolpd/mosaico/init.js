var mosaico = new Mosaico(new Config());
mosaico.render();
window.UOLI = window.UOLI || {};
window.UOLI[mosaico.getConfigTitle()] = {};
window.UOLI[mosaico.getConfigTitle()].clickTag = mosaico.clickTag;
window.UOLI[mosaico.getConfigTitle()].getClickTag = mosaico.getClickTag;
window.UOLI[mosaico.getConfigTitle()].loadComplete = mosaico.loadComplete;
window.UOLI[mosaico.getConfigTitle()].open = mosaico.open;
window.UOLI[mosaico.getConfigTitle()].close = mosaico.close;