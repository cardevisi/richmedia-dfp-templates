module.exports = function (grunt) {
    var dirs = require('./dirs');

    return {
        options: {
            banner: '(function (window, document, undefined){\n\'use strict\';\n',
            footer: '\n})(this, document);'
        },
        mosaico: {
            src: [
                '<%= dirs.src %>/uolpd/mosaico/Config.js',
                '<%= dirs.src %>/uolpd/mosaico/Align.js',
                '<%= dirs.src %>/uolpd/mosaico/Mosaico.js',
                '<%= dirs.src %>/uolpd/mosaico/init.js'
            ],
            dest: '<%= dirs.build %>/template-mosaico.js'
        }
    };
};
