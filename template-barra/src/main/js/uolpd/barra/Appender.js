function Appender () {

    var $public = this;
    var $private = {};

    $public.getContainerId = function () {
        return $private.containerId;
    };

    $public.setContainerId = function (containerId) {
        $private.containerId = containerId;
    };

    $public.getBanner = function () {
        return $private.banner;
    };

    $public.setBanner = function (banner) {
        $private.banner = banner;
    };

    $public.getIdControler = function () {
        return $private.idControler;
    };

    $public.setIdControler = function (idControler) {
        $private.idControler = idControler;
    };

    $public.getBackupCode = function () {
        return $private.backupCode;
    };

    $public.setBackupCode = function (backupCode) {
        $private.backupCode = backupCode;
    };

    $public.init = function () {
        if (top == self) {
            document.getElementById($private.containerId).appendChild($private.banner);
        } else {
            $private.insertOnParent();
        }
    };

    $private.insertOnParent = function () {
        try {
            var iframe = $private.getTopIframe();
            $private.injectBanner(iframe);
            $private.hideIframe(iframe);
        } catch (e) {
            document.body.appendChild($private.backupCode);
        }
    };

    $private.getTopIframe = function () {
        var topIframes = top.document.getElementsByTagName("IFRAME");
        for (var i = 0, length = topIframes.length; i < length; i++) {
            if (topIframes[i].contentWindow == self) {
                return topIframes[i];
            }
        }

        return;
    };

    $private.injectBanner = function (iframe) {
        var spanExist = top.document.getElementById($private.idControler);
        if (spanExist !== null) {
            return;
        }

        var span = $private.getSpan(iframe, $private.idControler);
        span.innerHTML = banner.outerHTML;
        iframe.parentNode.insertBefore(span, iframe);
    };

    $private.getSpan = function (iframe, idControler) {
        var span = top.document.createElement("span");
        span.style.display="inline-block";
        span.id = idControler;
        return $private.useStyle(span, iframe);
    };

    $private.useStyle = function (element, iframe) {
        var style = iframe.currentStyle || window.getComputedStyle(iframe);
        for (var key in style) {
            element.style[key] = style[key];
        }
        return element;
    };

    $private.hideIframe = function (iframe) {
        iframe.style.height = '0px';
        iframe.style.width = '0px';
    };

}