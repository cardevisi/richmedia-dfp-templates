function Barra (config) {

    var $public = this;
    var $private = {};

    var FIRST_IFRAME = 'uol-iframe-first-content-' + config.title;
    var SECOND_IFRAME = 'uol-iframe-second-content-' + config.title;
    var FIRST_CONTAINER = 'first-container-' + config.title;
    var SECOND_CONTAINER = 'second-container-' + config.title;
    var WRAPPER_ID = 'uol-richmedia-' + config.title;

    $private.appender = new Appender();

    $public.render = function() {
        var container = document.getElementById(config.bannerId);
        if(!container) {
            return;
        }

        container.appendChild($private.createWrapper(WRAPPER_ID));
        $private.writeIframeContent(FIRST_IFRAME, config.format.initial.src);
    };

    $private.createWrapper = function (name) {
        var first = $private.createFirstContent(config.format.initial.src, config.format.initial.width, config.format.initial.height);
        var second = $private.createSecondContent(config.format.expanded.src, config.format.expanded.width, config.format.expanded.height);
        var div = $private.createContainer(name);
        div.style.position = 'relative';
        div.appendChild(first);
        $private.appender.setContainerId(config.bannerId);
        $private.appender.setBanner(second.outerHTML);
        $private.appender.setIdControler(Math.random().toString(36).substring(2,9));
        return div;
    };

    $private.createFirstContent = function (src, width, height) {
        var div = $private.createContainer(FIRST_CONTAINER);
        $private.applyStyles(div, {'position' : 'relative', 'width' : width + 'px', 'height' : height + 'px', 'background' : config.format.initial.background});
        if(config.format.initial.mask === 'true') {
            div.appendChild($private.createMask(width, height, $private.open));
        }

        var iframe = $private.createIframe(FIRST_IFRAME, src, width, height);
        div.appendChild(iframe);
        $private.applyStyles(iframe, {'position' : 'absolute',
            'width' : iframe.parentNode.style.width, 'height' : iframe.parentNode.style.height,
            'top' : iframe.parentNode.style.top, 'right' : iframe.parentNode.style.right
        });

        return div;
    };

    $private.createContainer = function (name) {
        var div = document.createElement('div');
        div.setAttribute('id', name);
        return div;
    };

    $private.applyStyles = function (element, styles) {
        for (var style in styles){
            element.style[style] = styles[style];
        }
    };

    $private.open = function () {
        $private.toogleVisibility();
        $private.writeIframeContent(SECOND_IFRAME, config.format.expanded.src);
        $private.appender.init();
        $private.track('open', config.track.open);
    };

    $private.createIframe = function (id, src, width, height) {
        var iframe = document.createElement('iframe');
        iframe.setAttribute('id', id);
        iframe.setAttribute('width', width);
        iframe.setAttribute('height', height);
        iframe.setAttribute('marginwidth', '0');
        iframe.setAttribute('marginheight', '0');
        iframe.setAttribute('frameborder', '0');
        iframe.setAttribute('scrolling', 'no');
        return iframe;
    };

    $private.createSecondContent = function (src, width, height) {
        var div = $private.createContainer(SECOND_CONTAINER);
        $private.applyStyles(div, {'position' : 'absolute', 'width' : width + 'px', 'height' : height + 'px', 'top' : '0px', 'display' : 'none', 'zIndex' : 6000000});
        $private.showDirection(div, config.format.expanded.direction);
        if (config.format.expanded.mask === 'true') {
            div.appendChild($private.createMask(width, height, $public.clickTag));
        }

        var iframe = $private.createIframe(SECOND_IFRAME, src, width, height);
        div.appendChild(iframe);
        $private.applyStyles(iframe, {'position' : 'absolute',
            'width' : iframe.parentNode.style.width, 'height' : iframe.parentNode.style.height,
            'top' : iframe.parentNode.style.top, 'right' :iframe.parentNode.style.right
        });

        var btn = $private.createCloseButton();
        div.appendChild(btn);
        return div;
    };

    $private.showDirection = function (div, direction) {
        if(direction === 'right') {
            div.style.left = '0px';
        } else if(direction === 'left') {
            div.style.right = '0px';
        }
    };

    $private.createMask = function (width, height, callback) {
        var mask = document.createElement('a');
        $private.applyStyles(mask, {'position' : 'absolute', 'width' : width + 'px', 'height' : height + 'px', 'top' : '0', 'left' : '0', 'zIndex' : 1, 'cursor' : 'pointer'});
        $private.debugMask(mask);

        mask.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            callback();
        });

        return mask;
    };

    $private.debugMask = function (mask) {
        if (config.debug === 'true') {
            mask.style.background = '#ff0000';
            mask.style.opacity = 0.5;
        }
    };

    $public.clickTag = function () {
        $private.close();
        window.open($public.getClickTag(), '_blank');
    };

    $private.close = function () {
        $private.reloadIframe(FIRST_IFRAME);
        var div = document.getElementById(SECOND_CONTAINER);
        $private.toogleVisibility();
        div.style.background = config.format.expanded.background;
        $private.track('close', config.track.close);
        $private.reloadIframe(SECOND_IFRAME);
        setTimeout(function() {
           $private.writeIframeContent(FIRST_IFRAME, config.format.initial.src);
        }, 0);
    };

    $private.reloadIframe = function (id) {
        var iframe = document.getElementById(id);
        iframe.contentWindow.location.reload();
    };

    $private.toogleVisibility = function () {
        var div = document.getElementById(SECOND_CONTAINER);
        div.style.display = (div.style.display === 'none') ? 'block' : 'none';
    };

    $private.track = function (params, id) {
        if (id !== "" && id !== undefined) {
            var imgTracker = new Image();
            imgTracker.src = 'http://www5.smartadserver.com/imp?imgid=' + id + '&tmstp=' + (new Date()).getTime() + '&tgt=&' + params;
        }
    };

    $private.writeIframeContent = function (id, src) {
        var iframe = document.getElementById(id);
        iframe.contentWindow.document.open();
        iframe.contentWindow.document.write(src);
        iframe.contentWindow.document.close();
    };

    $public.getClickTag  = function (value) {
        var index = (value === undefined) ? 0 : value;
        return config.clickTag[index];
    };

    $private.createCloseButton = function () {
        var img = document.createElement('img');
        img.src = "http://bn.imguol.com/1110/uol/cutting/fechar.gif";

        var btn = document.createElement('a');
        btn.setAttribute('value', 'Fechar');
        $private.applyStyles(btn, {'position' : 'absolute', 'width' : '55px', 'height' : '15px', 'top' : '0', 'right' : '0px', 'zIndex' : 3, 'cursor' : 'pointer'});

        btn.addEventListener('click', function (e) {
            e.preventDefault();
            $private.close();
        });
        btn.appendChild(img);
        return btn;
    };
}