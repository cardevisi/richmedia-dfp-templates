function Config () {
    return {
        title : '%%TITLE%%',
        debug : 'false',
        format:{
            initial : {
                width : '%%WIDTH_INITIAL%%',
                height : '%%HEIGHT_INITIAL%%',
                mask : '%%MASK_INITIAL%%',
                src : '%%CLIENT_SRC_INITIAL%%',
                background : '%%BACKGROUND_INITIAL%%'
            },
            expanded: {
                width : '%%WIDTH_EXPANDED%%',
                height : '%%HEIGHT_EXPANDED%%',
                mask : '%%MASK_EXPANDED%%',
                src : '%%CLIENT_SRC_EXPANDED%%',
                background : '%%BACKGROUND_EXPANDED%%',
                direction: '%%DIRECTION%%'
            }
        },
        clickTag : [
            '%%CLICKTAG%%'
        ],
        track : {
            open  : '%%ID%%',
            close : '%%ID%%'
        },
        bannerId : '%%BANNER_ID%%'
    };
}