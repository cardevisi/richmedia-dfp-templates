#!/bin/bash

# import node modules repository
if [ `uname` == 'Linux' ]; then
    mkdir -p /export/jenkins/workspace/node_modules/richmedia-expandable-template 2> /dev/null || :
    cp -r /export/jenkins/workspace/node_modules/richmedia-expandable-template node_modules 2> /dev/null || :
fi

# execute grunt
npm install;
grunt -v;

# update node modules repository
if [ `uname` == 'Linux' ]; then
    cp -ru node_modules/* /export/jenkins/workspace/node_modules/richmedia-expandable-template 2> /dev/null || :
fi