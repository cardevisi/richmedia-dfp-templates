module.exports = function (grunt) {
    var dirs = require('./dirs');

    return {
        options: {
            banner: '(function (window, document, undefined){\n\'use strict\';\n',
            footer: '\n})(this, document);'
        },
        barra: {
            src: [
                '<%= dirs.src %>/uolpd/barra/Config.js',
                '<%= dirs.src %>/uolpd/barra/Appender.js',
                '<%= dirs.src %>/uolpd/barra/Barra.js',
                '<%= dirs.src %>/uolpd/barra/init.js'
            ],
            dest: '<%= dirs.build %>/template-barra.js'
        }
    };
};
