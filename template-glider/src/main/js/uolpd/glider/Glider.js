function Glider (configGlider) {
    
    var self = this;
    var zIndexOrigin;
    var countAnimation = 0;
    var FIRST_IFRAME, SECOND_IFRAME, FIRST_CONTAINER, SECOND_CONTAINER, WRAPPER_ID, GLIDER_WRAPPER, GLIDER_CONTENT, GLIDER_IFRAME, PRELOAD_CONTAINER;
    var config = configGlider;

    function configureNames () {
        FIRST_IFRAME = 'uol-iframe-first-content-' + config.title;
        SECOND_IFRAME = 'uol-iframe-second-content-' + config.title;
        FIRST_CONTAINER = 'first-container-' + config.title;
        SECOND_CONTAINER = 'second-container-' + config.title;
        GLIDER_WRAPPER = 'glider-wrapper';
        GLIDER_CONTENT = 'glider-content-' + config.title;
        PRELOAD_CONTAINER = 'preload-container-' + config.title;
        GLIDER_IFRAME = 'glider-iframe-second-' + config.title;
        WRAPPER_ID = 'uol-richmedia-' + config.title;
    }

    self.getConfigTitle = function () {
        return config.title.toUpperCase();
    };

    self.getClickTag  = function (value) {
        var index = (value === undefined) ? 0 : value;
        return config.clickTag[index];
    };
    
    self.clickTag = function () {
        self.close();
        try {
            window.open(self.getClickTag(), '_blank');
        } catch(e) {
            console.log('[CLICKTAG COM VALOR INVÁLIDO]');
        }
        return false;
    };

    self.open = function() {
        if (getBrowser() === 'msie 9.0' || getBrowser() === 'msie 10.0') {
            self.clickTag();
            return;
        }
        showGlider();
        track('open', config.track.open);
    };
    
    self.close = function() {
        closeAnimation();
        track('close', config.track.close);
    };

    self.loadComplete = function (element) {
        var id = element.getAttribute('id');
        removePreloader(id);
    };

    function getScrollHeight () {
        var doc = document;
        return Math.max(
            Math.max(doc.body.scrollHeight, doc.documentElement.scrollHeight),
            Math.max(doc.body.offsetHeight, doc.documentElement.offsetHeight),
            Math.max(doc.body.clientHeight, doc.documentElement.clientHeight)
        );
    }

    function getBrowserWidth() {
        var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        return width;
    }

    function addCssAnimation () {
        var style = document.createElement('style');
        style.setAttribute('type', 'text/css');
        var css = '@keyframes moveToBottom{from{top:0;left:0}to{top:'+ config.format.expanded.height +'px;left:0}}@keyframes moveToTop{from{top:'+ config.format.expanded.height +'px;left:0}to{top:0;left:0}}@keyframes rotateToOpen{from{-webkit-transform:perspective(1200px) rotateX(0);-moz-transform:perspective(1200px) rotateX(0);-o-transform:perspective(1200px) rotateX(0);-ms-transform:perspective(1200px) rotateX(0);transform:perspective(1200px) rotateX(0)}to{-webkit-transform:perspective(1200px) rotateX(20deg);-moz-transform:perspective(1200px) rotateX(20deg);-o-transform:perspective(1200px) rotateX(20deg);-ms-transform:perspective(1200px) rotateX(20deg);transform:perspective(1200px) rotateX(20deg)}}@keyframes rotateToClose{from{top:'+ config.format.expanded.height +'px;-webkit-transform:perspective(1200px) rotateX(0);-moz-transform:perspective(1200px) rotateX(0);-o-transform:perspective(1200px) rotateX(0);-ms-transform:perspective(1200px) rotateX(0);transform:perspective(1200px) rotateX(0)}to{top:'+ config.format.expanded.height +'px;-webkit-transform:perspective(1200px) rotateX(-20deg);-moz-transform:perspective(1200px) rotateX(-20deg);-o-transform:perspective(1200px) rotateX(-20deg);-ms-transform:perspective(1200px) rotateX(-20deg);transform:perspective(1200px) rotateX(-20deg)}}@keyframes stepAinit{from{-webkit-transform:perspective(1200px) rotateX(-20deg);-moz-transform:perspective(1200px) rotateX(-20deg);-o-transform:perspective(1200px) rotateX(-20deg);-ms-transform:perspective(1200px) rotateX(-20deg);transform:perspective(1200px) rotateX(-20deg)}to{-webkit-transform:perspective(1200px) rotateX(0);-moz-transform:perspective(1200px) rotateX(0);-o-transform:perspective(1200px) rotateX(0);-ms-transform:perspective(1200px) rotateX(0);transform:perspective(1200px) rotateX(0)}}@keyframes fadeIn{from{opacity:0}to{opacity:1}}@keyframes fadeOut{from{opacity:1}to{opacity:0}}.glider-wrapper{-webkit-perspective-origin:top,center;-moz-perspective-origin:top,center;-o-perspective-origin:top,center;-ms-perspective-origin:top,center;perspective-origin:top,center;-webkit-transform-origin:top center;-moz-transform-origin:top center;-o-transform-origin:top center;-ms-transform-origin:top center;transform-origin:top center;-webkit-transform-style:preserve-3d;-moz-transform-style:preserve-3d;-o-transform-style:preserve-3d;-ms-transform-style:preserve-3d;transform-style:preserve-3d;text-align:center}.open{animation:fadeIn 1s,rotateToOpen 1s,moveToBottom 1s forwards 1s,rotateToOpen 1s forwards reverse 1s;-webkit-animation:fadeIn 1s,rotateToOpen 1s,moveToBottom 1s forwards 1s,rotateToOpen 1s forwards reverse 1s;-moz-animation:fadeIn 1s,rotateToOpen 1s,moveToBottom 1s forwards 1s,rotateToOpen 1s forwards reverse 1s;-o-animation:fadeIn 1s,rotateToOpen 1s,moveToBottom 1s forwards 1s,rotateToOpen 1s forwards reverse 1s}.close{animation:rotateToClose 1s forwards,moveToTop 1s forwards 1s,stepAinit 1.2s forwards 1.2s,fadeOut 1s forwards 2s;-webkit-animation:rotateToClose 1s forwards,moveToTop 1s forwards 1s,stepAinit 1.2s forwards 1.2s,fadeOut 1s forwards 2s;-moz-animation:rotateToClose 1s forwards,moveToTop 1s forwards 1s,stepAinit 1.2s forwards 1.2s,fadeOut 1s forwards 2s;-o-animation:rotateToClose 1s forwards,moveToTop 1s forwards 1s,stepAinit 1.2s forwards 1.2s,fadeOut 1s forwards 2s}.showGliderContent{animation:fadeIn 1s;-webkit-animation:fadeIn 1s;-moz-animation:fadeIn 1s;-o-animation:fadeIn 1s}.hideGliderContent{animation:fadeOut .5s;-webkit-animation:fadeOut .5s;-moz-animation:fadeOut .5s;-o-animation:fadeOut .5s}';
        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }
        var head = document.head || document.getElementsByTagName('head')[0];
        head.appendChild(style);
    }
    
    function reloadIframe (id) {
        var iframe = document.getElementById(id);
        iframe.setAttribute('src', '');
    }

    function toogleVisibility (id) {
        var div = document.getElementById(id);
        div.style.display = (div.style.display === 'none') ? 'block' : 'none';
    }

    function toogleContainerZindex (id) {
        var div = document.getElementById(id);
        
        if(!zIndexOrigin) {
            zIndexOrigin = div.style.zIndex;
        }

        if (zIndexOrigin === '' || zIndexOrigin === '999') {
            div.style.zIndex = '2147483647';
        } else {
            div.style.zIndex = zIndexOrigin;
        }
    }

    function track (params, id) {
        var url, path;
        var timestamp = (new Date()).getTime();
        var imgTracker = new Image();
        path = "http://www5.smartadserver.com/imp?imgid=";

        if (id === "" || id === undefined) {
            return;
        }

        url = path+id+"&tmstp="+timestamp+"&tgt=&"+params;
        imgTracker.src = url;
    }

    function createIframe (id) {
        var iframe = document.createElement('iframe');
        iframe.setAttribute('id', id);
        iframe.setAttribute('marginwidth', '0');
        iframe.setAttribute('marginheight', '0');
        iframe.setAttribute('frameborder', '0');
        iframe.setAttribute('scrolling', 'no');
        return iframe;
    }

    function writeIframeContent (id, src) {
        var iframe = document.getElementById(id);
        //iframe.setAttribute('onload', 'window.UOLI.'+self.getConfigTitle()+'.loadComplete(this);');
        iframe.contentWindow.document.open();
        iframe.contentWindow.document.write(src);
        iframe.contentWindow.document.close();
    }


    function createPreloader () {
        var img = 'http://ci.i.uol.com.br/album/wait_inv.gif';
        var image = new Image();
        
        applyStyles(image, {
            'position' : 'absolute', 
            'top' : '50%', 
            'left' : '50%', 
            'zIndex' : 1, 
            'margin' : '-11px 0 0 -11px'
        });

        image.setAttribute('id', PRELOAD_CONTAINER);
        image.setAttribute('src', img);
        return image;
    }

    function getContainerById (id) {
        var div, idFound;
        if (id.indexOf('first') > -1) {
            idFound = FIRST_CONTAINER;
        } else if (id.indexOf('second') > -1) {
            idFound = GLIDER_CONTENT;
        }
        return document.getElementById(idFound);
    }
    
    function addPreloader (id) {
        var div = getContainerById(id);
        try {
            var preloadImage = createPreloader();
            div.appendChild(preloadImage);
        } catch(e) {}
    }

    function removePreloader (id) {
        var div = getContainerById(id);
        var preloadImage = document.getElementById(PRELOAD_CONTAINER);
        try {
            div.removeChild(preloadImage);
        } catch(e) {}
    }

    function sourceIframeContent (id, url) {
        var iframe = document.getElementById(id);
        iframe.setAttribute('onload', 'window.UOLI.'+self.getConfigTitle()+'.loadComplete(this);');
        iframe.setAttribute('src', url);
    }

    function loadIframeContent (id, src) {
        addPreloader(id);
        if (config.format.write.toString() === 'true') {
            if (typeof src !== 'string') {
                console.log('[UOL RICHMEDIA] src em modo write incorreto.');
                return;
            }
            writeIframeContent(id, src);
        } else {
            sourceIframeContent(id, src);
        }
    }

    function createCloseButton () {
        var img = document.createElement('img');
        img.src = "http://bn.imguol.com/1110/uol/cutting/fechar.gif";

        var btn = document.createElement('a');
        btn.setAttribute('value', 'Fechar');

        var posX = (getBrowserWidth() * 0.5) - (config.format.expanded.width * 0.5);

        applyStyles(btn, {
            'position' : 'absolute', 
            'width' : '55px', 
            'height' : '15px', 
            'top' : '5px',
            'right' : posX+'px', 
            'zIndex' : 3, 'cursor' : 'pointer'
        });

        btn.addEventListener('click', function (e) {
            e.preventDefault();
            self.close();
        });
        btn.appendChild(img);
        return btn;
    }

    function applyStyles (element, styles) {
        for (var style in styles){
            element.style[style] = styles[style];
        }
    }

    function createFirstContent(width, height) {
        var div = createContainer(FIRST_CONTAINER);
        
        applyStyles(div, {
            'position' : 'relative', 
            'width' : width + 'px', 
            'height' : height + 'px',
            'background' : config.format.initial.background
        });

        if(config.format.initial.mask.toString() === 'true') {
            div.appendChild(createMask(width, height, 'false', self.open));
        }

        var iframe = createIframe(FIRST_IFRAME);
        div.appendChild(iframe);
        
        applyStyles(iframe, {
            'position' : 'absolute',
            'width' : iframe.parentNode.style.width, 
            'height' : iframe.parentNode.style.height,
            'top' : iframe.parentNode.style.top, 
            'right' : iframe.parentNode.style.right
        });

        return div;
    }

    function createSecondContent () {
        var div = createContainer(GLIDER_WRAPPER);
        div.setAttribute('class', GLIDER_WRAPPER);
        
        applyStyles(div, {
            'position' : 'absolute', 
            'width' : '100%', 
            'height' : 
            '1600px', 
            'top' : '0px', 
            'display' : 'none',
            'background' : config.format.expanded.background
        });

        var img = document.createElement('img');
        img.src = config.screenshot;
        
        applyStyles(img, {
            'position' : 'relative', 
            'marginRight' : '-50%', 
            'marginLeft' : '-50%'
        });

        div.appendChild(img);
        div.appendChild(createMask('100%', '100%', 'true', self.close));

        var wrapper = createSecondWrapper(SECOND_CONTAINER);
        wrapper.appendChild(div);
        
        var glider = createGliderContent(config.format.expanded.width, config.format.expanded.height);
        wrapper.appendChild(glider);

        var btn = createCloseButton();
        wrapper.appendChild(btn);

        return wrapper;
    }

    function createSecondWrapper (id) {
        var wrapper = createContainer(id);
        applyStyles(wrapper, {
            'position' : 'fixed', 
            'width' : '100%', 
            'height' : getScrollHeight() + 'px', 
            'top' : '0px', 
            'left' : '0px', 
            'display' : 'none',
            'background' : config.format.expanded.background,
            'zIndex': 1
        });
        return wrapper;
    }

    function createGliderContent (width, height) {
        var div = createContainer(GLIDER_CONTENT);
        applyStyles(div, {
            'position' : 'relative', 
            'width' : width + 'px', 
            'height' : height + 'px', 
            'top' : '0px', 
            'left' : '0px', 
            'display' : 'none',
            'margin' : '0 auto',
            'background' : config.format.expanded.background
        });
        if (config.format.expanded.mask.toString() === 'true') {
            div.appendChild(createMask(width, height, 'true', self.clickTag));
        }

        var iframe = createIframe(GLIDER_IFRAME);
        div.appendChild(iframe);
        
        applyStyles(iframe, {
            'position' : 'absolute',
            'width' : iframe.parentNode.style.width, 
            'height' : iframe.parentNode.style.height,
            'top' : iframe.parentNode.style.top, 
            'right' : iframe.parentNode.style.right
        });

        return div;
    }

    function createContainer (name) {
        var div = document.createElement('div');
        div.setAttribute('id', name);
        return div;
    }

    function createMask (width, height, flag, callback) {
        var mask = document.createElement('a');
        
        applyStyles(mask, {
            'position' : 'absolute', 
            'top' : '0', 
            'left' : '0', 
            'zIndex' : 1, 
            'cursor' : 'pointer', 
            'display' : 'block'
        });

        if (flag === 'true') {
            mask.style.width = '100%';
            mask.style.height = '100%';
        } else {
            mask.style.width = width + 'px';
            mask.style.height = height + 'px';
        }
        debugMask(mask);

        mask.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            callback();
        });
        return mask;
    }

    function debugMask (mask) {
        if (config.debug.toString() === 'true') {
            applyStyles(mask, {'background' : '#ff0000', 'opacity' : 0.5});
        }
    }

    function showGlider () {
        window.scrollTo(0,0);
        toogleContainerZindex(config.bannerId);
        toogleVisibility(SECOND_CONTAINER);
        toogleVisibility(GLIDER_WRAPPER);
        applyClass(GLIDER_WRAPPER, 'open');
        var wrapper = document.getElementById(GLIDER_WRAPPER);
        wrapper.addEventListener('animationend', animationEndToOpen);
        wrapper.addEventListener('webkitAnimationEnd', animationEndToOpen);
        wrapper.addEventListener('MSAnimationEnd', animationEndToOpen);
        wrapper.addEventListener('oAnimationEnd', animationEndToOpen);
    }

    function animationEndToOpen (e) {
        countAnimation ++;
        var condition;

        if (getBrowser() ==='firefox' || getBrowser() === 'msie 10.0' || getBrowser() === 'rv:11.0') {
            condition = (countAnimation == 3);
        } else {
            condition = (countAnimation == 4);
        }

        if (e.animationName === 'rotateToOpen' && condition) {
            toogleVisibility(GLIDER_CONTENT);
            loadIframeContent(GLIDER_IFRAME, config.format.expanded.src);
            applyClass(GLIDER_CONTENT, 'showGliderContent', true);
            this.removeEventListener('animationend', animationEndToOpen);
            this.removeEventListener('webkitAnimationEnd', animationEndToOpen);
            this.removeEventListener('MSAnimationEnd', animationEndToOpen);
            this.removeEventListener('oAnimationEnd', animationEndToOpen);
            countAnimation = 0;
        }
    }

    function getBrowser () {
        var nav = navigator.userAgent; 
        var name, initPos;
        var navName = ['Firefox', 'MSIE 10.0', 'rv:11.0', 'Chrome', 'MSIE 9.0'];
        for (var i = 0; i < navName.length; i++) {
            initPos = nav.indexOf(navName[i]);
            if (initPos === -1) {
                continue;
            }
            name = nav.substr(initPos, navName[i].length);
        }
        return name.toLowerCase();
    }

    function animationEndToCloseGliderContent (e) {
        if (e.animationName === 'fadeOut') {
            toogleVisibility(GLIDER_CONTENT);
            var wrapper = document.getElementById(GLIDER_WRAPPER);
            wrapper.addEventListener('animationend', animationEndToCloseSecondContainer);
            wrapper.addEventListener('webkitAnimationEnd', animationEndToCloseSecondContainer);
            wrapper.addEventListener('MSAnimationEnd', animationEndToCloseSecondContainer);
            wrapper.addEventListener('oAnimationEnd', animationEndToCloseSecondContainer);
            this.removeEventListener('animationend', animationEndToCloseGliderContent);
            this.removeEventListener('webkitAnimationEnd', animationEndToCloseGliderContent);
            this.removeEventListener('MSAnimationEnd', animationEndToCloseGliderContent);
            this.removeEventListener('oAnimationEnd', animationEndToCloseGliderContent);
            applyClass(GLIDER_WRAPPER, 'close');
        }
    }

    function animationEndToCloseSecondContainer (e) {
        if (e.animationName === 'fadeOut') {
            reloadIframe(GLIDER_IFRAME);
            toogleVisibility(SECOND_CONTAINER);
            toogleVisibility(GLIDER_WRAPPER);
            toogleContainerZindex(config.bannerId);
            this.removeEventListener('animationend', animationEndToCloseSecondContainer);
            this.removeEventListener('webkitAnimationEnd', animationEndToCloseSecondContainer);
            this.removeEventListener('MSAnimationEnd', animationEndToCloseSecondContainer);
            this.removeEventListener('oAnimationEnd', animationEndToCloseSecondContainer);
        }
    }

    function closeAnimation () {
        applyClass(GLIDER_CONTENT, 'hideGliderContent', true);
        var gliderContent = document.getElementById(GLIDER_CONTENT);
        gliderContent.addEventListener('animationend', animationEndToCloseGliderContent);
        gliderContent.addEventListener('webkitAnimationEnd', animationEndToCloseGliderContent);
        gliderContent.addEventListener('MSAnimationEnd', animationEndToCloseGliderContent);
        gliderContent.addEventListener('oAnimationEnd', animationEndToCloseGliderContent);
    }

    function applyClass (id, className, override) {
        var element = document.getElementById(id);
        var classValue = element.getAttribute('class');
        if (classValue && !override) {
            classValue = classValue.trim();
            classValue = classValue.split(' ');
            element.setAttribute('class', classValue[0] + ' ' + className);
            return;
        }
        element.setAttribute('class', className);
    }

    self.render = function() {
        var container = document.getElementById(config.bannerId);
        if(!container) {
            return;
        }

        addCssAnimation();
        configureNames(config);
        var count = 0;

        window.addEventListener("scroll", function(e){
            var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
            var secondContainer = document.getElementById(SECOND_CONTAINER);
            secondContainer.style.top = -scrollTop + 'px';
        });

        var first = createFirstContent(config.format.initial.width, config.format.initial.height);
        var second = createSecondContent();
        var wrapper = createWrapper(WRAPPER_ID);
        wrapper.appendChild(first);
        wrapper.appendChild(second);
        container.appendChild(wrapper);
        loadIframeContent(FIRST_IFRAME, config.format.initial.src);
    };

    function createWrapper (name) {
        var div = createContainer(name);
        div.style.position = 'relative';
        return div;
    }
}