function Config () {
    return {
        title : '@@%%TITLE%%',
        debug: true,
        screenshot : '@@%%SCREENSHOT%%',
        format: {
            initial: {
                width : '%%WIDTH_INITIAL%%', // largura do formato inicial
                height : '%%HEIGHT_INITIAL%%', // altura do formato inicial
                mask : '%%MASK_INITIAL%%', // em true habilita a mascara - false desabilita
                background : '@@%%BACKGROUND_INITIAL%%', // cor de fundo Ex. #000000
                src : '@@%%SOURCE_INITIAL%%', //path do html ou variavel do conteudo html
                reload: false //recarrega a primeira peca
            },
            expanded : {
                width : '%%WIDTH_EXPANDED%%', //largura do formato expandido
                height : '%%HEIGHT_EXPANDED%%', //altura do formato expandido
                mask : '%%MASK_EXPANDED%%', // em true habilita a mascara - false desabilita
                background : '@@%%BACKGROUND_EXPANDED%%',
                src : '@@%%SOURCE_EXPANDED%%', //path do html ou variavel do conteudo html
                reload: false //recarrega a segunda peca
            },
            write : '%%WRITE%%'
        },
        clickTag : [
            '%%CLICKTAG%%'
        ],
        track : {
            open : '%%ID_OPEN%%',
            close : '%%ID_CLOSE%%'
        },
        bannerId : '@@%%BANNER_ID%%' // banner ID do container
    };
}