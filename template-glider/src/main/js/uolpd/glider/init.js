var glider = new Glider(new Config());
glider.render();
window.UOLI = window.UOLI || {};
window.UOLI[glider.getConfigTitle()] = {};
window.UOLI[glider.getConfigTitle()].clickTag = glider.clickTag;
window.UOLI[glider.getConfigTitle()].getClickTag = glider.getClickTag;
window.UOLI[glider.getConfigTitle()].loadComplete = glider.loadComplete;
window.UOLI[glider.getConfigTitle()].open = glider.open;
window.UOLI[glider.getConfigTitle()].close = glider.close;