module.exports = function (grunt) {
    var dirs = require('./dirs');
    var version = '\/*\n* © UOL RICHMEDIA - Todos os direitos reservados\n* VERSAO: @@version_value\n*\/';


    return {
        options: {
            banner: '(function (window, document, undefined){\n\'use strict\';\n'+version+'\n',
            footer: '\n})(this, document);'
        },
        glider: {
            src: [
                '<%= dirs.src %>/uolpd/glider/Config.js',
                '<%= dirs.src %>/uolpd/glider/Align.js',
                '<%= dirs.src %>/uolpd/glider/Glider.js',
                '<%= dirs.src %>/uolpd/glider/init.js'
            ],
            dest: '<%= dirs.build %>/template-glider.js'
        }
    };
};
