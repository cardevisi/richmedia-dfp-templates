module.exports = function (grunt) {
    var dirs = require('./dirs');

    return {
        dist: {
            src: '<%= dirs.htmlSrc %>/index.html',
            dest: '<%= dirs.build %>/index.html',
            options: {
                beautify: true,
                sections: {
                    layout: {
                        header: '<%= dirs.htmlSrc %>/header.html'
                    }
                },
                data: {
                    script: 'template-viewport.js',
                    title: "UOL Richmedia Template Viewport",
                }
            }
        }
    };
};