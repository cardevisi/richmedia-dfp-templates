module.exports = function (grunt) {
    var dirs = require('./dirs');

    return {
        all: [
            '<%= dirs.src %>/**/*.js',
            '<%= dirs.test %>/**/*.js'
        ]
    };
};