module.exports = function (grunt) {
    var dirs = require('./dirs');

    return {
        options: {
            banner: '(function (window, document, undefined){\n\'use strict\';\n',
            footer: '\n})(this, document);'
        },
        viewport: {
            src: [
                '<%= dirs.src %>/uolpd/viewport/Config.js',
                '<%= dirs.src %>/uolpd/viewport/Viewport.js',
                '<%= dirs.src %>/uolpd/viewport/init.js'
            ],
            dest: '<%= dirs.build %>/template-viewport.js'
        }
    };
};
