function Config () {
    return {
        title : '%%TITLE%%',
        debug : 'false',
        format:{
            initial : {
                width : '%%WIDTH_INITIAL%%',
                height : '%%HEIGHT_INITIAL%%',
                mask : '%%MASK_INITIAL%%',
                src : '%%CLIENT_SRC_INITIAL%%',
                background : '%%BACKGROUND_INITIAL%%',
                eventMouseenter : '%%EVENT_MOUSE%%',
                opening : {
                    top : '%%TOP_OPENING%%',
                    left : '%%LEFT_OPENING%%',
                    width : '%%WIDTH_OPENING%%',
                    height : '%%HEIGHT_OPENING%%',
                    timeOpen : '%%TIME_OPENING%%'
                }
            },
            expanded: {
                mask : '%%MASK_EXPANDED%%',
                src : '%%CLIENT_SRC_EXPANDED%%',
                background : '%%BACKGROUND_EXPANDED%%'
            }
        },
        clickTag : [
            '%%CLICKTAG%%'
        ],
        track : {
            open  : '%%ID%%',
            close : '%%ID%%'
        },
        bannerId : '%%BANNER_ID%%'
    };
}