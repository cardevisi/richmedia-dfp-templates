function Viewport (configViewport) {
    var self = this;

    var flagTimeOpen, FIRST_IFRAME, SECOND_IFRAME, FIRST_CONTAINER, SECOND_CONTAINER, WRAPPER_ID, isOpened;
    var config = configViewport;

    function configureNames () {
        FIRST_IFRAME = 'uol-iframe-first-content-' + config.title;
        SECOND_IFRAME = 'uol-iframe-second-content-' + config.title;
        FIRST_CONTAINER = 'first-container-' + config.title;
        SECOND_CONTAINER = 'second-container-' + config.title;
        WRAPPER_ID = 'uol-richmedia-' + config.title;
        isOpened = 'isOpened' + config.title;
    }

    self.clickTag = function () {
        close();
        window.open(self.getClickTag(), '_blank');
    };

    self.getClickTag  = function (value) {
        var index = (value === undefined) ? 0 : value;
        return config.clickTag[index];
    };

    function close () {
        reloadIframe(FIRST_IFRAME);
        var div = document.getElementById(SECOND_CONTAINER);
        toogleVisibility();
        div.style.background = config.format.expanded.background;
        track('close', config.track.close);
        reloadIframe(SECOND_IFRAME);
        setTimeout(function() {
           writeIframeContent(FIRST_IFRAME, config.format.initial.src);
        }, 0);
    }

    function reloadIframe (id) {
        var iframe = document.getElementById(id);
        iframe.contentWindow.location.reload();
        if (config.format.initial.eventMouseenter === 'true') {
            reloadMask();
        }
    }

    function reloadMask () {
        var session = window.sessionStorage.getItem(isOpened);
        var mask = document.getElementById('initial-mask');
        if (session === 'open') {
            maskTotal(mask, 'absolute');
        } else {
            maskMouseenter(mask, 'absolute');
        }
    }

    function toogleVisibility () {
        var div = document.getElementById(SECOND_CONTAINER);
        div.style.display = (div.style.display === 'none') ? 'block' : 'none';
    }

    function open () {
        toogleVisibility();
        writeIframeContent(SECOND_IFRAME, config.format.expanded.src);
        track('open', config.track.open);
    }

    function track (params, id) {
        var url, path;
        var timestamp = (new Date()).getTime();
        var imgTracker = new Image();
        path = "http://www5.smartadserver.com/imp?imgid=";

        if (id === "" || id === undefined) {
            return;
        }

        url = path+id+"&tmstp="+timestamp+"&tgt=&"+params;
        imgTracker.src = url;
    }

    function createIframe (id) {
        var iframe = document.createElement('iframe');
        iframe.setAttribute('id', id);
        iframe.setAttribute('marginwidth', '0');
        iframe.setAttribute('marginheight', '0');
        iframe.setAttribute('frameborder', '0');
        iframe.setAttribute('scrolling', 'no');
        return iframe;
    }

    function writeIframeContent (id, src) {
        var iframe = document.getElementById(id);
        iframe.contentWindow.document.open();
        iframe.contentWindow.document.write(src);
        iframe.contentWindow.document.close();
    }

    function createCloseButton () {
        var img = document.createElement('img');
        img.src = "http://bn.imguol.com/1110/uol/cutting/fechar.gif";

        var btn = document.createElement('a');
        btn.setAttribute('value', 'Fechar');
        applyStyles(btn, {'position' : 'absolute', 'width' : '55px', 'height' : '15px', 'top' : '0', 'right' : '0px', 'zIndex' : 3, 'cursor' : 'pointer'});

        btn.addEventListener('click', function (e) {
            e.preventDefault();
            close();
        });
        btn.appendChild(img);
        return btn;
    }

    function applyStyles (element, styles) {
        for (var style in styles){
                element.style[style] = styles[style];
            }
    }

    function createFirstContent(src, width, height) {
        var div = createContainer(FIRST_CONTAINER);
        applyStyles(div, {'position' : 'relative', 'width' : width + 'px', 'height' : height + 'px',
            'background' : config.format.initial.background});
        if(config.format.initial.mask === 'true') {
            var mask = createMask('initial-mask');
            var session = window.sessionStorage.getItem(isOpened);
            if (config.format.initial.eventMouseenter === 'true' && session === null) {
                div.appendChild(maskMouseenter(mask, 'absolute'));
            } else {
                div.appendChild(maskTotal(mask, 'absolute'));
            }
        }

        var iframe = createIframe(FIRST_IFRAME);
        div.appendChild(iframe);
        applyStyles(iframe, {'position' : 'absolute',
            'width' : iframe.parentNode.style.width, 'height' : iframe.parentNode.style.height,
            'top' : iframe.parentNode.style.top, 'right' : iframe.parentNode.style.right
        });

        return div;
    }

    function createSecondContent (src) {
        var div = createContainer(SECOND_CONTAINER);
        applyStyles(div, {'position' : 'fixed', 'width' : '100%', 'height' : '100%', 'top' : '0px', 'left' : '0px', 'display' : 'none',
            'zIndex' : 7000000, 'background' : config.format.expanded.background});
        if (config.format.expanded.mask === 'true') {
            var mask = createMask('second-mask');
            div.appendChild(secondMask(mask, 'fixed', self.clickTag));
        }

        var iframe = createIframe(SECOND_IFRAME);
        div.appendChild(iframe);
        applyStyles(iframe, {'position' : 'fixed',
            'width' : iframe.parentNode.style.width, 'height' : iframe.parentNode.style.height,
            'top' : iframe.parentNode.style.top, 'right' : iframe.parentNode.style.right
        });

        var btn = createCloseButton();
        div.appendChild(btn);
        return div;
    }

    function createContainer (name) {
        var div = document.createElement('div');
        div.setAttribute('id', name);
        return div;
    }

    function createMask (id) {
        var mask = document.createElement('a');
        mask.setAttribute('id', id);
        return mask;
    }

    function maskTotal (mask, position) {
        mask.removeEventListener('mouseenter', eventMouseEnter);
        mask.removeEventListener('mouseleave', eventMouseLeave);
        applyStyles(mask, {'position' : position, 'width' : '100%', 'height' : '100%', 'top' : '0', 'left' : '0', 'zIndex' : 1, 'cursor' : 'pointer'});
        debugMask(mask);

        mask.addEventListener('click', eventClick);
        return mask;
    }

    function eventClick (e) {
        e.preventDefault();
        e.stopPropagation();
        open();
    }

    function maskMouseenter (mask, position) {
        applyStyles(mask, {'position' : position, 'width' : config.format.initial.opening.width + 'px',
            'height' : config.format.initial.opening.height + 'px', 'top' : config.format.initial.opening.top + 'px',
            'left' : config.format.initial.opening.left + 'px', 'zIndex' : 1, 'cursor' : 'pointer'
        });
        debugMask(mask);

        mask.addEventListener('mouseenter', eventMouseEnter);
        mask.addEventListener('mouseleave', eventMouseLeave);
        return mask;
    }

    function eventMouseEnter (e) {
        e.preventDefault();
        e.stopPropagation();
        flagTimeOpen = setTimeout(function() {
            open();
            window.sessionStorage.setItem(isOpened, 'open');
        }, config.format.initial.opening.timeOpen);
    }

    function eventMouseLeave (e) {
        e.preventDefault();
        e.stopPropagation();
        clearTimeout(flagTimeOpen);
    }

    function secondMask (mask, position, callback) {
        applyStyles(mask, {'position' : position, 'width' : '100%', 'height' : '100%', 'top' : '0', 'left' : '0', 'zIndex' : 1, 'cursor' : 'pointer'});
        debugMask(mask);

        mask.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            callback();
        });
        return mask;
    }

    function debugMask (mask) {
        if (config.debug === 'true') {
            mask.style.background = '#ff0000';
            mask.style.opacity = 0.5;
        }
    }

    self.render = function() {
        var container = document.getElementById(config.bannerId);
        if(!container) {
            return;
        }
        configureNames(config);

        var first = createFirstContent(config.format.initial.src, config.format.initial.width, config.format.initial.height);
        var second = createSecondContent(config.format.expanded.src, config.format.expanded.width, config.format.expanded.height);
        var wrapper = createWrapper(WRAPPER_ID);
        wrapper.appendChild(first);
        wrapper.appendChild(second);
        container.appendChild(wrapper);
        writeIframeContent(FIRST_IFRAME, config.format.initial.src);
    };

    function createWrapper (name) {
        var div = createContainer(name);
        div.style.position = 'relative';
        return div;
    }
}